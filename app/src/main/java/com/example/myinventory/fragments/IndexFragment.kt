package com.example.myinventory.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myinventory.MainActivity
import com.example.myinventory.R
import com.example.myinventory.adapters.ProductAdapter
import com.example.myinventory.models.Product
import com.google.firebase.firestore.FirebaseFirestore

class IndexFragment : Fragment(), ProductAdapter.OnItemClickListener {

    /**
     *  Este fragmento lo manejo yo, en este se va a manejar la lista de productos que c/u
     *  llevará al fragmento de detalles (todavía no existe XDDDD).
     *
     *  Todavía no se implementa Firebase para empezar a guardar info, pero ahí andan los
     *  modelos de datos que se van a usar.
     */

    private lateinit var productsList: RecyclerView
    private lateinit var adapter: ProductAdapter
    private lateinit var products: ArrayList<Product>
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_index, container, false)

        productsList = view.findViewById(R.id.products_list)
        products = arrayListOf()
        getProducts()

        return view
    }

    private fun getProducts() {
        db.collection("products").get().addOnCompleteListener {  task ->

            if (task.isSuccessful) {
                products.clear()

                for (product in task.result?.documents!!) {

                    @Suppress("UNCHECKED_CAST")
                    products.add(
                        Product(product["name"] as String,
                            product["code"] as Long,
                            product["category"].toString().toInt(),
                            product["costUSD"].toString().toDouble(),
                            product["costMXN"].toString().toDouble(),
                            product["quantity"].toString().toInt(),
                            product["description"] as String,
                            product["ingredients"] as String,
                            product["main_benefits"] as ArrayList<String>,
                            product["details"] as ArrayList<String>,
                            product["image"] as String,
                            product.id
                        )
                    )
                }

                adapter = ProductAdapter(products,this)
                productsList.layoutManager = LinearLayoutManager(view?.context)
                productsList.adapter = adapter
            } else Toast.makeText((activity as MainActivity), "NO FUE POSIBLE OBTENER LA LISTA DE PRODUCTOS", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onItemClick(position: Int) {
        val parent = (activity as MainActivity)

        parent.productDetailFragment = ProductDetailFragment.newInstance(products[position].id)

        parent.loadFragment(parent.productDetailFragment)
    }

//    companion object {
//        fun newInstance() = IndexFragment()
//    }
}