package com.example.myinventory.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.myinventory.R
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

class ProductDetailFragment : Fragment() {

    private var id = "id"

    private lateinit var productImage: ImageView
    private lateinit var lbProductName: MaterialTextView
    private lateinit var lbProductCode: MaterialTextView
    private lateinit var lbProductUSD: MaterialTextView
    private lateinit var lbProductMXN: MaterialTextView
    private lateinit var lbProductCategory: MaterialTextView
    private lateinit var lbProductQuantity: MaterialTextView
    private lateinit var lbProductDetails: MaterialTextView
    private lateinit var lbProductDescription: MaterialTextView
    private lateinit var lbProductBenefits: MaterialTextView
    private lateinit var lbProductIngredients: MaterialTextView
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val storage: FirebaseStorage = FirebaseStorage.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_product_detail, container, false)

        productImage = view.findViewById(R.id.product_image)
        lbProductName = view.findViewById(R.id.product_name)
        lbProductCode = view.findViewById(R.id.product_code)
        lbProductUSD = view.findViewById(R.id.product_price_usd)
        lbProductMXN = view.findViewById(R.id.product_price_mxn)
        lbProductCategory = view.findViewById(R.id.product_category)
        lbProductQuantity = view.findViewById(R.id.product_quantity)
        lbProductDetails = view.findViewById(R.id.product_details)
        lbProductDescription = view.findViewById(R.id.product_description)
        lbProductBenefits = view.findViewById(R.id.product_benefits)
        lbProductIngredients = view.findViewById(R.id.product_ingredients)

        getProduct(id)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getString("PRODUCT_ID")?.let {
            id = it
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun getProduct(id: String) {

        db.collection("products").document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {

                lbProductName.text = getString(R.string.product_name, task.result?.get("name").toString())
                lbProductCode.text = getString(R.string.product_code, task.result?.get("code").toString())
                lbProductUSD.text = getString(R.string.product_price_usd, task.result?.get("costUSD").toString())
                lbProductMXN.text = getString(R.string.product_price_mxn, task.result?.get("costMXN").toString())
                lbProductCategory.text = getString(R.string.product_category, task.result?.get("category").toString())
                lbProductQuantity.text = getString(R.string.product_quantity, task.result?.get("quantity").toString())
                lbProductDescription.text = getString(R.string.product_description, task.result?.get("description").toString())
                lbProductIngredients.text = getString(R.string.product_ingredients, task.result?.get("ingredients").toString())
                val detailsList: ArrayList<String> = task.result?.get("details") as ArrayList<String>
                val benefitsList: ArrayList<String> = task.result?.get("main_benefits") as ArrayList<String>
                var str = ""

                for (t in detailsList.indices) {
                    str += "- ${detailsList[t]}"
                    if (t < 10) str += "\n\n"
                }
                Log.i("DETALLES", str)
                lbProductDetails.text = getString(R.string.product_details, str)

                str = ""
                for (s in benefitsList.indices) {
                    str += "- ${benefitsList[s]}"
                    if (s < 2) str += "\n\n"
                }
                Log.i("BENEFICIOS", str)
                lbProductBenefits.text = getString(R.string.product_benefits, str)

                @Suppress("NAME_SHADOWING")
                storage.reference.child("products/${task.result?.get("image")}").downloadUrl.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        //Log.i("IMAGE", task.result.toString())
                        this.view?.let {
                            Glide.with(it.context)
                                .load(task.result)
                                .into(productImage)
                        }
                    }
                }
            }
        }
    }

    companion object {

        fun newInstance(id: String) =
            ProductDetailFragment().apply {
                arguments = Bundle().apply {
                    putString("PRODUCT_ID", id)
                }
            }
    }
}