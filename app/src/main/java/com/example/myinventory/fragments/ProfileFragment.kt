package com.example.myinventory.fragments

import android.app.Activity.RESULT_OK
import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.myinventory.MainActivity
import com.example.myinventory.R
import com.example.myinventory.models.Employee
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView


@Suppress("PrivatePropertyName")
class ProfileFragment : Fragment() {

    private lateinit var profilePhoto: ImageView
    private lateinit var tfName: EditText
    private lateinit var tfLastName: EditText
    private lateinit var tfPosition: EditText
    private lateinit var tfWorkHours: EditText
    private lateinit var tfRole: EditText
    private lateinit var tfContract: EditText
    private lateinit var tfEmail: EditText
    private lateinit var tfPassword: EditText
    private lateinit var tfAbout: EditText
    private lateinit var buttonEdit :Button
    private lateinit var buttonGuardar :Button

    /**
     *  Reference to Firebase Authentication: provides user UID, email;
     *  functions to update email, password, account recuperation and more!
     */
    private lateinit var auth: FirebaseAuth

    /**
     *  Reference to Firebase Firestore: provides access to database for execution of Creation, Read, Update and Delete functions
     */
    private lateinit var db: FirebaseFirestore

    /**
     *  Reference to Firebase Storage: provides access to database storage for images and documents retrieving
     */
    private lateinit var storage: FirebaseStorage

    private lateinit var user: Employee

    private val GALLERY_REQ_CODE = 1234

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()

        profilePhoto = view.findViewById(R.id.imageProfile)
        tfName = view.findViewById(R.id.editTextName)
        tfLastName = view.findViewById(R.id.editTextLastName)
        tfPosition = view.findViewById(R.id.editTextPosition)
        tfWorkHours = view.findViewById(R.id.editTextWorkingHours)
        tfRole = view.findViewById(R.id.editTextRole)
        tfContract = view.findViewById(R.id.editTextContractDuration)
        tfEmail = view.findViewById(R.id.editTextEmail)
        tfPassword = view.findViewById(R.id.editTextPassword)
        tfAbout = view.findViewById(R.id.editTextAbout)
        buttonEdit = view.findViewById(R.id.buttonEdit)
        buttonGuardar = view.findViewById(R.id.buttonGuardar)

        //Modificables por el admin
        tfPosition.isEnabled = false
        tfWorkHours.isEnabled = false
        tfRole.isEnabled = false
        tfContract.isEnabled = false

        //Modificables por el usuario
        buttonGuardar.isEnabled = false
        tfName.isEnabled = false
        tfLastName.isEnabled =false
        tfEmail.isEnabled = false
        tfPassword.isEnabled = false
        tfAbout.isEnabled = false

        buttonEdit.setOnClickListener{
            buttonGuardar.isEnabled = true
            tfName.isEnabled = true
            tfLastName.isEnabled = true
            tfEmail.isEnabled = true
            tfPassword.isEnabled = true
            tfAbout.isEnabled = true
            buttonEdit.isEnabled = false

        }

        buttonGuardar.setOnClickListener{

            val userRef = db.collection("user_information").document(user.email)


            userRef.update("about", tfAbout.text.toString(),
                    "last_name" , tfLastName.text.toString(),
                    "name", tfName.text.toString())
                    .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
                    .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }

            buttonEdit.isEnabled = true
            buttonGuardar.isEnabled = false
            tfName.isEnabled = false
            tfLastName.isEnabled = false
            tfEmail.isEnabled = false
            tfPassword.isEnabled = false
            tfAbout.isEnabled = false

            //updateInformation()
        }

        profilePhoto.setOnClickListener {
            pickFromGallery()
        }

        setInformation()

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            GALLERY_REQ_CODE -> {
                if (resultCode == RESULT_OK) {

                    data?.data?.let { uri ->

                        launchCropImage(uri)
                    }
                } else Log.d("ERROR", "-- IMAGE SELECTION ERROR")
            }

            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)

                if (resultCode == RESULT_OK) {
                    result.uri?.let { uri->
                        setImage(uri)
                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Log.d("ERROR", "-- ${result.error}")
                }
            }
        }

    }

    private fun launchCropImage(uri: Uri) {

        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1000,1000)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start((activity as MainActivity).baseContext,this)
    }

    private fun setImage(uri: Uri) {
        //uploadImage(uri,"${auth.currentUser?.email}/profile/main.jpg")

        profilePhoto.setImageURI(uri)
    }

    /**
     *  Pass all employee information to UI fields
     */
    private fun setUIFields() {

        tfName.hint = user.name
        tfLastName.hint = user.lastName
        tfPosition.hint = user.position
        tfWorkHours.hint = user.workHours
        tfRole.hint = user.role
        tfContract.hint = user.contractDuration
        tfEmail.hint = user.email
        tfAbout.setText(user.about)
    }

    /**
     *  Retrieves employee information of Firestore and initializes 'user: Employee' object, then calls setUIFields function
     */
    private fun setInformation() {

        db.collection("user_information").document(auth.currentUser!!.email!!).addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w("ERROR", "Listen failed.", e)
                Toast.makeText(this.context,"Hubo un problema al obtener los datos: \n ${e.message}",Toast.LENGTH_SHORT).show()
                return@addSnapshotListener
            }

            if (snapshot != null) {

                //Log.i("USER_INFO", snapshot.toString())

                user = Employee(snapshot["name"].toString(),
                    snapshot["last_name"].toString(),
                    snapshot["position"].toString(),
                    snapshot["work_hours"].toString(),
                    snapshot["role"].toString(),
                    snapshot["contract_duration"].toString(),
                    auth.currentUser!!.email!!,
                    snapshot["phone"].toString(),
                    snapshot["about"].toString()
                )

                setUIFields()
            }
        }
    }

//    private fun updateInformation() {
//
//        val docRef = db.collection("user_information").document("email")
//
//        docRef.addSnapshotListener { snapshot, e ->
//            if (e != null) {
//                Log.w(TAG, "Listen failed.", e)
//                return@addSnapshotListener
//            }
//
//            if (snapshot != null && snapshot.exists()) {
//
//                Log.d(TAG, "Current data: ${snapshot.data}")
//
//                db.collection("user_information").document(tfEmail.toString()).set(
//                        hashMapOf(
//                                "about" to tfAbout.text.toString(),
//                                "contract_duration" to tfContract.text.toString(),
//                                "last_name" to tfLastName.text.toString(),
//                                "name" to tfName.text.toString(),
//                        )
//                )
//
//            } else {
//                Log.d(TAG, "Current data: null")
//            }
//        }
//    }

    private fun pickFromGallery() {

        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg","image/png","image/jpg")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        startActivityForResult(intent, GALLERY_REQ_CODE)
    }

}