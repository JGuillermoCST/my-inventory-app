package com.example.myinventory.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myinventory.R
import com.example.myinventory.models.Product
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.storage.FirebaseStorage

class ProductAdapter(private val products: ArrayList<Product>,private val listener: OnItemClickListener): RecyclerView.Adapter<ProductAdapter.ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ProductHolder (layoutInflater.inflate(R.layout.card_product, parent, false))
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.render(products[position])
    }

    override fun getItemCount(): Int = products.size

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    inner class ProductHolder(private val view: View):RecyclerView.ViewHolder(view), View.OnClickListener {

        private val productImage: ImageView = view.findViewById(R.id.product_image)
        private val productName: MaterialTextView = view.findViewById(R.id.product_name)
        private val productAvailability: MaterialTextView = view.findViewById(R.id.product_availability)
        private val storage: FirebaseStorage = FirebaseStorage.getInstance()

        init {
            view.setOnClickListener(this)
        }

        fun render(product: Product) {

            var str = "Disponibles: "
            str += product.quantity.toString()

            productName.text = product.name
            productAvailability.text = str
            storage.reference.child("products/${product.image}").downloadUrl.addOnCompleteListener { task ->

                if (task.isSuccessful) {
                    //Log.i("IMAGE", task.result.toString())
                    Glide.with(this.view.context)
                        .load(task.result)
                        .into(productImage)
                }
            }
        }

        override fun onClick(v: View?) {
            val pos:Int = adapterPosition
            if (pos != RecyclerView.NO_POSITION) listener.onItemClick(pos)
        }
    }
}