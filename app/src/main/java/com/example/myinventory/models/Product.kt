package com.example.myinventory.models

data class Product(var name: String,
                   var code: Long,
                   var category: Int,
                   var costUSD: Double,
                   var costMXN: Double,
                   var quantity: Int,
                   var description: String,
                   var ingredients: String,
                   var benefits: ArrayList<String>,
                   var details: ArrayList<String>,
                   var image: String,
                   val id: String)
