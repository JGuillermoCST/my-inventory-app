package com.example.myinventory.models

import java.text.FieldPosition

data class Employee(var name: String,
                    var lastName: String,
                    var position: String,
                    var workHours: String,
                    var role: String,
                    var contractDuration: String,
                    var email: String,
                    var phone: String,
                    var about: String)
