package com.example.myinventory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.myinventory.fragments.IndexFragment
import com.example.myinventory.fragments.LicensesFragment
import com.example.myinventory.fragments.ProductDetailFragment
import com.example.myinventory.fragments.ProfileFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val drawerLayout: DrawerLayout by lazy { findViewById(R.id.drawer_layout) }
    private val appBar: MaterialToolbar by lazy { findViewById(R.id.tool_bar) }
    private val navigationDrawer: NavigationView by lazy { findViewById(R.id.nav_view) }

    private val indexFragment: IndexFragment = IndexFragment()
    private val profileFragment: ProfileFragment = ProfileFragment()
    private val licensesFragment: LicensesFragment = LicensesFragment()
    var productDetailFragment: ProductDetailFragment = ProductDetailFragment()

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    private lateinit var toggler: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toggler = ActionBarDrawerToggle(this,drawerLayout,appBar,R.string.drawer_open,R.string.drawer_close)
        drawerLayout.addDrawerListener(toggler)
        toggler.isDrawerIndicatorEnabled = true
        toggler.syncState()
        navigationDrawer.setNavigationItemSelectedListener(this)
        loadFragment(indexFragment)
    }

    override fun onBackPressed() {

        if (productDetailFragment.isVisible || profileFragment.isVisible || licensesFragment.isVisible) loadFragment(indexFragment)

        if (indexFragment.isVisible) {
            Toast.makeText(this,"Sesión cerrada",Toast.LENGTH_LONG).show()
            auth.signOut()
            startActivity(Intent(this,LoginActivity::class.java))
        }
    }

    /**
     *  Esta función sirve para manejar los fragmentos que contienen las interfaces;
     *  cuando se llama la función sólo se le pasa la variable del fragmento a jalar
     */
    fun loadFragment(fragment: Fragment) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment).addToBackStack( "tag" )
        transaction.commit()
    }

    /**
     *  Esta función sobreescrita es para manejar las selecciones del Navigation Drawer
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        drawerLayout.closeDrawers()
        when (item.itemId) {

            R.id.home_view -> {
                appBar.title = "My inventory app"
                loadFragment(indexFragment)
                return true
            }

            R.id.account_view -> {
                appBar.title = "Mi cuenta"
                loadFragment(profileFragment)
                return true
            }

            R.id.licenses_view -> {
                appBar.title = "Licencias"
                loadFragment(licensesFragment)
                return true
            }

            R.id.terminate_session -> {
                auth.signOut()
                Toast.makeText(this,"Sesión cerrada",Toast.LENGTH_LONG).show()
                startActivity(Intent(this,LoginActivity::class.java))
                return true
            }

            else -> {
                appBar.title = getString(R.string.app_name)
                loadFragment(indexFragment)
                return true
            }
        }
    }
}