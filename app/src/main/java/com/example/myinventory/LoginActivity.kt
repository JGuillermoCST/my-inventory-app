package com.example.myinventory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.button.MaterialButton
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private val tfEmail: EditText by lazy { findViewById<EditText>(R.id.tf_email) }
    private val tfPassword: EditText by lazy { findViewById<EditText>(R.id.tf_password) }
    private val btnLogin: MaterialButton by lazy { findViewById<MaterialButton>(R.id.btn_login) }

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener { submitData() }
    }

    private fun submitData() {

        if (checkData()) {

            val data: Array<String> = arrayOf(tfEmail.text.toString(), tfPassword.text.toString())

            auth.signInWithEmailAndPassword(data[0],data[1])
                .addOnCompleteListener { task ->

                    if (task.isSuccessful) {
                        startActivity(Intent(this,MainActivity::class.java))
                    } else {
                        Toast.makeText(this, "Error: ${task.exception?.message}", Toast.LENGTH_LONG).show()
                    }
                }
                .addOnFailureListener { error ->
                    Toast.makeText(this, "Error: ${error.message}", Toast.LENGTH_LONG).show()
                }
        } else Toast.makeText(this, "Debe ingresar usuario y contraseña", Toast.LENGTH_LONG).show()
    }

    private fun checkData(): Boolean {
        return !tfEmail.text.equals("") && !tfPassword.text.equals("")
    }
}